﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.Services;
using Common.DTOs.Team;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamsController : ControllerBase
    {
        private readonly TeamService teamService;

        public TeamsController(TeamService teamService)
        {
            this.teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<ICollection<TeamDTO>>> Get()
        {
            return Ok(await teamService.GetAllTeams());
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetById(int id)
        {
            return Ok(await teamService.GetTeamById(id));
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> Create([FromBody] TeamCreateDTO dto)
        {
            return Ok(await teamService.CreateTeam(dto));
        }

        [HttpPut]
        public async Task<ActionResult<TeamDTO>> Put([FromBody] TeamUpdateDTO team)
        {
            return Ok(await teamService.UpdateTeam(team));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteById(int id)
        {
            await teamService.DeleteTeamById(id);
            return NoContent();
        }
    }
}
