using BLL;
using BLL.Services;
using DAL.Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using System;
using System.Linq;

namespace WebAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //var cxt = new DAL.Context.ModelsContext().Deserialize();
            //var rep = new Repository<Team>(cxt);
            ////var team = new Team { Name = "Fuck", CreatedAt = DateTime.Now };
            //var team = cxt.Teams.Where(item => item.Id == 1).FirstOrDefault();
            //rep.Delete(team);
            CreateHostBuilder(args).Build().Run();

        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
