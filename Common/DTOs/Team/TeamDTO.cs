﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Team
{
    public class TeamDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
