﻿using Common.DTOs.TaskModel;
using Common.DTOs.Team;
using Common.DTOs.User;
using System;
using System.Collections.Generic;
using System.Text;

namespace Common.DTOs.Project
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }

        public ICollection<TaskModelDTO> Tasks { get; set; }

        public TeamDTO? Team { get; set; }
        public UserDTO? Author { get; set; }
    }
}
