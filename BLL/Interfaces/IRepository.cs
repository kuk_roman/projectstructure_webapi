﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace BLL.Interfaces
{
    public interface IRepository<TEntity> where TEntity: BaseEntity
    {
        void Create(TEntity entity, string createdBy = null);
        void CreateRange(ICollection<TEntity> entities, string createdBy = null);
        void Update(TEntity entity, string updatedBy = null);
        void DeleteById(int id);
        void Delete(TEntity entity);
        void DeleteRange(ICollection<TEntity> entities);
        ICollection<TEntity> GetAll();
        TEntity GetById(int id);
    }
}
