﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUnitOfWork
    {
        IRepository<TEntity> Set<TEntity>() where TEntity : BaseEntity;
        void SaveChanges();
        Task SaveChangesAsync();
    }
}
