﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Services.Abstract;
using Common.DTOs.FunctionalModels;
using Common.DTOs.Team;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TeamService : BaseService
    {
        private readonly IRepository<Team> repository;

        public TeamService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper)
        {
            this.repository = unitOfWork.Set<Team>();
        }

        public async Task<ICollection<TeamDTO>> GetAllTeams()
        {
            var teams = await Task.Factory.StartNew(() => repository.GetAll());

            return mapper.Map<ICollection<TeamDTO>>(teams);
        }

        public async Task<TeamDTO> GetTeamById(int id)
        {
            var teamEntity = await Task.Factory.StartNew(() => repository.GetById(id));
            if (teamEntity == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            return mapper.Map<TeamDTO>(teamEntity);
        }

        public async Task<TeamDTO> CreateTeam(TeamCreateDTO teamDto)
        {
            var teamEntity = mapper.Map<Team>(teamDto);
            teamEntity.CreatedAt = DateTime.Now;

            repository.Create(teamEntity);
            await unitOfWork.SaveChangesAsync();

            var teamDTO = mapper.Map<TeamDTO>(teamEntity);
            return teamDTO;
        }

        public async Task<TeamDTO> UpdateTeam(TeamUpdateDTO teamDto)
        {
            var teamEntity = repository.GetById(teamDto.Id);
            if (teamEntity == null)
            {
                throw new NotFoundException(nameof(Team), teamDto.Id);
            }
            var teamEntityMap = mapper.Map<Team>(teamDto);

            teamEntityMap.CreatedAt = teamEntity.CreatedAt;

            repository.Update(teamEntityMap);
            await unitOfWork.SaveChangesAsync();

            var updatedTeamDto = mapper.Map<TeamDTO>(teamEntityMap);

            return updatedTeamDto;
        }

        public async Task DeleteTeamById(int id)
        {
            var teamEntity = repository.GetById(id);
            if (teamEntity == null)
            {
                throw new NotFoundException(nameof(Team), id);
            }

            repository.DeleteById(id);
            await unitOfWork.SaveChangesAsync();
        }

    }
}
