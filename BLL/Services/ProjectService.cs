﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Services.Abstract;
using Common.DTOs.FunctionalModels;
using Common.DTOs.Project;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ProjectService : BaseService
    {
        private readonly IRepository<Project> repository;
        private readonly TeamService teamService;
        private readonly UserService userService;

        public ProjectService(IUnitOfWork unitOfWork, IMapper mapper,
            TeamService teamService, UserService userService) : base(unitOfWork, mapper)
        {
            this.repository = unitOfWork.Set<Project>();
            this.teamService = teamService;
            this.userService = userService;
        }

        public async Task<ICollection<ProjectDTO>> GetAllProjects()
        {
            var projects = await Task.Factory.StartNew(() => repository.GetAll());

            return mapper.Map<ICollection<ProjectDTO>>(projects);
        }

        public async Task<ProjectDTO> GetProjectById(int id)
        {
            var projectEntity = await Task.Factory.StartNew(() => repository.GetById(id));
            if (projectEntity == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }

            return mapper.Map<ProjectDTO>(projectEntity);
        }

        public async Task<ProjectDTO> CreateProject(ProjectCreateDTO projectDto)
        {
            var projectEntity = mapper.Map<Project>(projectDto);
            projectEntity.CreatedAt = DateTime.Now;

            repository.Create(projectEntity);
            await unitOfWork.SaveChangesAsync();

            var projectDTO = mapper.Map<ProjectDTO>(projectEntity);
            return projectDTO;
        }

        public async Task<ProjectDTO> UpdateProject(ProjectUpdateDTO projectDto)
        {
            var projectEntity = repository.GetById(projectDto.Id);
            if (projectEntity == null)
            {
                throw new NotFoundException(nameof(Project), projectDto.Id);
            }
            var projectEntityMap = mapper.Map<Project>(projectDto);

            if (teamService.GetTeamById(projectEntity.TeamId) == null)
            {
                throw new NotFoundException(nameof(Team), projectEntity.TeamId);
            }
            if (userService.GetUserById(projectEntity.AuthorId) == null)
            {
                throw new NotFoundException(nameof(User), projectEntity.AuthorId);
            }

            projectEntityMap.CreatedAt = projectEntity.CreatedAt;
            repository.Update(projectEntityMap);
            await unitOfWork.SaveChangesAsync();

            var updatedTaskDto = mapper.Map<ProjectDTO>(projectEntityMap);

            return updatedTaskDto;
        }

        public async Task DeleteProjectById(int id)
        {
            var projectEntity = repository.GetById(id);
            if (projectEntity == null)
            {
                throw new NotFoundException(nameof(Project), id);
            }

            //var tasks = (await taskService.GetAllTasks()).Where(task => task.ProjectId == id);
            //foreach (var task in tasks)
            //    await taskService.DeleteTaskById(task.Id);

            //if (projectEntity.Tasks.Count != 0)
            //{
            //    foreach (var task in projectEntity.Tasks)
            //        projectEntity.Tasks.Remove(task);
            //}
            //projectEntity.Tasks.Clear();

            repository.DeleteById(id);
            await unitOfWork.SaveChangesAsync();
        }

        public async Task<Dictionary<ProjectDTO, int>> GetUserProjectTasksCountByUserId(int userId)
        {
            var projects = (await GetAllProjects())
                .Where(pr => pr.AuthorId == userId)
                .GroupBy(pr => pr)
                .ToDictionary(item => item.Key, item => item.Key.Tasks.Count());

            return projects;
        }

        public async Task<IEnumerable<Project_TasksDTO>> CreateNewProjectStructureAsync()
        {
            var teams = await teamService.GetAllTeams();
            var users = await userService.GetAllUsers();
            var structure = (await GetAllProjects())
                .Join(teams,
                    project => project.TeamId,
                    team => team.Id,
                    (pr, t) => new Project_TasksDTO
                    {
                        Project = pr,
                        LongestTaskByDescription = pr.Tasks
                            .OrderByDescending(task => task.Description.Length).FirstOrDefault(),
                        ShortestTaskByName = pr.Tasks
                            .OrderBy(task => task.Name.Length).FirstOrDefault(),
                        TotalUsersCount = teams
                        .Where(team => team.Id == t.Id)
                        .GroupJoin(users,
                            team => team.Id,
                            user => user.TeamId,
                            (t, u) =>
                            {
                                if (pr.Description.Length > 20 || pr.Tasks.Count() < 3)
                                    return u.Count();
                                else
                                    return 0;
                            })
                        .FirstOrDefault()
                    });
            return structure;
        }
    }
}
