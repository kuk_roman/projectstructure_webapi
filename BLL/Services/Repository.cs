﻿using BLL.Interfaces;
using DAL.Context;
using DAL.Entities.Abstract;
using System.Collections.Generic;
using System.Linq;

namespace BLL.Services
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        protected readonly ModelsContext context;

        public Repository(ModelsContext context)
        {
            this.context = context;
        }
        
        public void Create(TEntity entity, string createdBy = null)
        {
            if (entity.Id == 0)
                entity.Id = context.Set<TEntity>().LastOrDefault().Id + 1;
            context.Set<TEntity>().Add(entity);
        }

        public void CreateRange(ICollection<TEntity> entities, string createdBy = null)
        {
            foreach (var item in entities)
                Create(item);
        }

        public void DeleteById(int id)
        {
            TEntity entity = context.Set<TEntity>().Where(item => item.Id.Equals(id)).FirstOrDefault();
            context.Set<TEntity>().Remove(entity);
        }

        public void Delete(TEntity entity)
        {
            context.Set<TEntity>().Remove(entity);
        }

        public void DeleteRange(ICollection<TEntity> entities)
        {
            foreach (var item in entities)
                Delete(item);
        }

        public void Update(TEntity entity, string updatedBy = null)
        {
            TEntity oldEntity = context.Set<TEntity>().Where(item => item.Id.Equals(entity.Id)).FirstOrDefault();
            var collection = context.Set<TEntity>();
            collection[collection.IndexOf(oldEntity)] = entity;
        }

        public ICollection<TEntity> GetAll()
        {
            return context.Set<TEntity>();
        }

        public TEntity GetById(int id)
        {
            return context.Set<TEntity>().Where(item => item.Id.Equals(id)).FirstOrDefault();
        }
    }
}
