﻿using BLL.Interfaces;
using DAL.Context;
using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        protected readonly ModelsContext context;

        public UnitOfWork(ModelsContext context)
        {
            this.context = context;
        }
        public void SaveChanges()
        {
            context.Serialize();
        }

        public async Task SaveChangesAsync()
        {
            await context.SerializeAsync();
        }

        public IRepository<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return new Repository<TEntity>(context);
        }
    }
}
