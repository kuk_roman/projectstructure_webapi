﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Services.Abstract;
using Common.DTOs.FunctionalModels;
using Common.DTOs.TaskModel;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TaskService : BaseService
    {
        private readonly IRepository<TaskModel> repository;
        private readonly ProjectService projectService;
        private readonly UserService userService;

        public TaskService(IUnitOfWork unitOfWork, IMapper mapper, 
            ProjectService projectService, UserService userService) : base(unitOfWork, mapper)
        {
            this.repository = unitOfWork.Set<TaskModel>();
            this.projectService = projectService;
            this.userService = userService;
        }

        public async Task<ICollection<TaskModelDTO>> GetAllTasks()
        {
            var tasks = await Task.Factory.StartNew(() => repository.GetAll());

            return mapper.Map<ICollection<TaskModelDTO>>(tasks);
        }

        public async Task<TaskModelDTO> GetTaskById(int id)
        {
            var taskEntity = await Task.Factory.StartNew(() => repository.GetById(id));
            if (taskEntity == null)
            {
                throw new NotFoundException(nameof(TaskModel), id);
            }

            return mapper.Map<TaskModelDTO>(taskEntity);
        }

        public async Task<TaskModelDTO> CreateTask(TaskModelCreateDTO taskDto)
        {
            var taskEntity = mapper.Map<TaskModel>(taskDto);
            taskEntity.CreatedAt = DateTime.Now;
            taskEntity.State = TaskState.Created;

            if (projectService.GetProjectById(taskEntity.ProjectId) == null)
            {
                throw new NotFoundException(nameof(Project), taskEntity.ProjectId);
            }
            if (userService.GetUserById(taskEntity.PerformerId) == null)
            {
                throw new NotFoundException(nameof(User), taskEntity.PerformerId);
            }
            repository.Create(taskEntity);

            var taskDTO = mapper.Map<TaskModelDTO>(taskEntity);

            (await projectService.GetProjectById(taskEntity.ProjectId)).Tasks.Add(taskDTO);
            await unitOfWork.SaveChangesAsync();
            return taskDTO;
        }

        public async Task<TaskModelDTO> UpdateTask(TaskModelUpdateDTO taskDto)
        {
            var taskEntity = repository.GetById(taskDto.Id);
            if (taskEntity == null)
            {
                throw new NotFoundException(nameof(TaskModel), taskDto.Id);
            }
            var taskEntityMap = mapper.Map<TaskModel>(taskDto);

            if (projectService.GetProjectById(taskEntity.ProjectId) == null)
            {
                throw new NotFoundException(nameof(Project), taskEntity.ProjectId);
            }
            if (userService.GetUserById(taskEntity.PerformerId) == null)
            {
                throw new NotFoundException(nameof(User), taskEntity.PerformerId);
            }

            taskEntityMap.CreatedAt = taskEntity.CreatedAt;
            repository.Update(taskEntityMap);
            await unitOfWork.SaveChangesAsync();

            var updatedTaskDto = mapper.Map<TaskModelDTO>(taskEntityMap);

            return updatedTaskDto;
        }

        public async Task DeleteTaskById(int id)
        {
            var taskEntity = repository.GetById(id);
            if (taskEntity == null)
            {
                throw new NotFoundException(nameof(TaskModel), id);
            }

            repository.DeleteById(id);
            await unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<TaskModelDTO>> GetTasksByUserIdWithNameConditionAsync(int userId)
        {
            var tasks = (await projectService.GetAllProjects())
                .SelectMany(p => p.Tasks
                    .Where(task => task.PerformerId == userId && task.Name.Length < 45)
                );

            return tasks;
        }

        public async Task<IEnumerable<TaskFinishedThisYearDTO>> GetTasksByUserFinishedThisYear(int userId)
        {
            var tasks = (await projectService.GetAllProjects())
                .SelectMany(p => p.Tasks
                    .Where(task => task.PerformerId == userId &&
                        task.FinishedAt.Year == DateTime.Now.Year/*2020*/ &&
                        task.State == TaskState.Finished)
                    .Select(task => new TaskFinishedThisYearDTO { Id = task.Id, Name = task.Name })
                );

            return tasks;
        }

        public async Task<IEnumerable<User_TasksDTO>> GetUsersSortedByFirstNameAndSortedTasksAsync()
        {
            var users = (await userService.GetAllUsers())
                .GroupJoin(await GetAllTasks(),
                    user => user.Id,
                    task => task.PerformerId,
                    (u, t) => new User_TasksDTO
                    {
                        User = u,
                        Tasks = t.OrderByDescending(task => task.Name.Length).ToList()
                    })
                .OrderBy(id => id.User.FirstName);

            return users;
        }

        public async Task<IEnumerable<UserLastProjectTasksDTO>> CreateNewUserStructureAsync(int userId)
        {
            var users = await userService.GetAllUsers();
            var tasks = await GetAllTasks();
            var structure = users
                .Where(u => u.Id == userId)
                .GroupJoin(await projectService.GetAllProjects(),
                    user => user.Id,
                    project => project.AuthorId,
                    (us, pr) => new UserLastProjectTasksDTO
                    {
                        User = us,
                        LastProject = pr
                            .OrderByDescending(p => p.CreatedAt)
                            .FirstOrDefault(),
                        LastProjectTasksCount = pr
                            .OrderByDescending(p => p.CreatedAt)
                            .FirstOrDefault()?
                            .Tasks.Count(),
                        UndoneOrCanceledTasksCount = users
                            .Where(user => user.Id == userId)
                            .GroupJoin(tasks,
                                user => user.Id,
                                task => task.PerformerId,
                                (u, t) => t.Count(task => task.State != TaskState.Finished))
                            .FirstOrDefault(),
                        LongestTask = users
                            .Where(user => user.Id == userId)
                            .GroupJoin(tasks,
                                user => user.Id,
                                task => task.PerformerId,
                                (u, t) => t.OrderByDescending(task => task.FinishedAt - task.CreatedAt).FirstOrDefault())
                            .FirstOrDefault(),
                    });

            return structure;
        }
    }
}
