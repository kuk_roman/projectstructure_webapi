﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Services.Abstract;
using Common.DTOs.FunctionalModels;
using Common.DTOs.User;
using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : BaseService
    {
        private readonly IRepository<User> repository;
        private readonly TeamService teamService;

        public UserService(IUnitOfWork unitOfWork, IMapper mapper, TeamService teamService) : base(unitOfWork, mapper)
        {
            this.repository = unitOfWork.Set<User>();
            this.teamService = teamService;
        }

        public async Task<ICollection<UserDTO>> GetAllUsers()
        {
            var users = await Task.Factory.StartNew(() => repository.GetAll());

            return mapper.Map<ICollection<UserDTO>>(users);
        }

        public async Task<UserDTO> GetUserById(int id)
        {
            var userEntity = await Task.Factory.StartNew(() => repository.GetById(id));
            if (userEntity == null)
            {
                throw new NotFoundException(nameof(User), id);
            }

            return mapper.Map<UserDTO>(userEntity);
        }

        public async Task<UserDTO> CreateUser(UserCreateDTO userDto)
        {
            var userEntity = mapper.Map<User>(userDto);
            userEntity.RegisteredAt = DateTime.Now;

            if (teamService.GetTeamById(userEntity.TeamId ?? 0) == null)
            {
                throw new NotFoundException(nameof(Team), userEntity.TeamId ?? 0);
            }

            repository.Create(userEntity);
            await unitOfWork.SaveChangesAsync();

            var userDTO = mapper.Map<UserDTO>(userEntity);
            return userDTO;
        }

        public async Task<UserDTO> UpdateUser(UserUpdateDTO userDto)
        {
            var userEntity = repository.GetById(userDto.Id);
            if (userEntity == null)
            {
                throw new NotFoundException(nameof(User), userDto.Id);
            }
            var userEntityMap = mapper.Map<User>(userDto);

            if (teamService.GetTeamById(userEntityMap.TeamId ?? 0) == null)
            {
                throw new NotFoundException(nameof(Team), userEntityMap.TeamId ?? 0);
            }

            userEntityMap.RegisteredAt = userEntity.RegisteredAt;

            repository.Update(userEntityMap);
            await unitOfWork.SaveChangesAsync();

            var updatedTeamDto = mapper.Map<UserDTO>(userEntityMap);

            return updatedTeamDto;
        }

        public async Task DeleteUserById(int id)
        {
            var userEntity = repository.GetById(id);
            if (userEntity == null)
            {
                throw new NotFoundException(nameof(User), id);
            }

            repository.DeleteById(id);
            await unitOfWork.SaveChangesAsync();
        }

        public async Task<IEnumerable<Team_UsersDTO>> GetTeamsUsersSortedByRegisteredDateAsync()
        {
            var teams = (await teamService.GetAllTeams())
                        .GroupJoin(await GetAllUsers(),
                            team => team.Id,
                            user => user.TeamId,
                            (t, u) =>
                            {
                                return new Team_UsersDTO
                                {
                                    Id = t.Id,
                                    Name = t.Name,
                                    Users = u
                                        .OrderByDescending(us => us.RegisteredAt)
                                        .ToList()
                                };

                            })
                        .Where(id => id.Users
                                        .All(user => DateTime.Now.Year - user.Birthday.Year > 10) &&
                                        id.Users.Count() != 0);

            return teams;
        }
    }
}
