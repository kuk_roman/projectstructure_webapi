﻿using Collections_LINQ.Models;
using Collections_LINQ.Models.FunctionModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Collections_LINQ.Interfaces
{
    interface IProjectService
    {
        Task<ICollection<Project>> GetProjects();
        Task<Project> GetProjectById(int id);
        Task<Project> CreateProject(Project project);
        Task<Project> UpdateProject(Project project);
        Task DeleteProject(string id);
        Task<IEnumerable<Project_Tasks>> GetNewProjectStructure();
        Task<Dictionary<Project, int>> GetUserProjectTasksCountByUserId(int id);
    }
}
