﻿using Collections_LINQ.Models;
using Collections_LINQ.Models.FunctionModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Collections_LINQ.Interfaces
{
    interface IUserService
    {
        Task<ICollection<User>> GetUsers();
        Task<User> GetUserById(int id);
        Task<User> CreateUser(User user);

        Task<User> UpdateUser(User user);

        Task DeleteUser(string id);

        Task<IEnumerable<Team_Users>> GetTeamsUsersSortedByRegisteredDate();
    }
}
