﻿using Collections_LINQ.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Collections_LINQ.Interfaces
{
    interface ITeamService
    {
        Task<ICollection<Team>> GetTeams();
        Task<Team> GetTeamById(int id);
        Task<Team> CreateTeam(Team team);

        Task<Team> UpdateTeam(Team team);

        Task DeleteTeam(string id);
    }
}
