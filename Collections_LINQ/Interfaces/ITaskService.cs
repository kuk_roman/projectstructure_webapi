﻿using Collections_LINQ.Models;
using Collections_LINQ.Models.FunctionModels;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Collections_LINQ.Interfaces
{
    interface ITaskService
    {
        Task<ICollection<TaskModel>> GetTasks();
        Task<TaskModel> GetTaskById(int id);

        Task<TaskModel> CreateTask(TaskModel task);

        Task<TaskModel> UpdateTask(TaskModel task);

        Task DeleteTask(string id);

        Task<IEnumerable<TaskModel>> GetTasksByUserIdWithNameCondition(int id);

        Task<IEnumerable<TaskFinishedThisYear>> GetTasksByUserFinishedThisYear(int id);

        Task<IEnumerable<User_Tasks>> GetUsersSortedByFirstNameAndSortedTasks();

        Task<IEnumerable<UserLastProjectTasks>> GetNewUserStructure(int id);
    }
}
