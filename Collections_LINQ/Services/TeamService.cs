﻿using Collections_LINQ.Interfaces;
using Collections_LINQ.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ.Services
{
    class TeamService : ITeamService
    {
        private readonly string baseUrl;
        private readonly HttpClient client;

        public TeamService(string baseUrl, HttpClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<ICollection<Team>> GetTeams()
        {
            var teams = await client.GetStringAsync($"{baseUrl}/api/teams");
            return JsonConvert.DeserializeObject<ICollection<Team>>(teams);
        }

        public async Task<Team> GetTeamById(int id)
        {
            var team = await client.GetStringAsync($"{baseUrl}/api/teams/{id}");
            return JsonConvert.DeserializeObject<Team>(team);
        }

        public async Task<Team> CreateTeam(Team team)
        {
            var response = await client.PostAsJsonAsync($"{baseUrl}/api/teams", team);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<Team>();
        }

        public async Task<Team> UpdateTeam(Team team)
        {
            var response = await client.PutAsJsonAsync($"{baseUrl}/api/teams", team);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<Team>();
        }

        public async Task DeleteTeam(string id)
        {
            var response = await client.DeleteAsync($"{baseUrl}/api/teams/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
        }
    }
}
