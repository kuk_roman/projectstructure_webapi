﻿using Collections_LINQ.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Net.Http;
using System.Linq;
using System.Threading.Tasks;
using Collections_LINQ.Models.FunctionModels;
using Collections_LINQ.Interfaces;
using System.Runtime.CompilerServices;

namespace Collections_LINQ.Services
{
    class AppService
    {
        private readonly IProjectService _projectService;
        private readonly ITaskService _taskService;
        private readonly ITeamService _teamService;
        private readonly IUserService _userService;
        private ICollection<Project> Projects { get; set; }
        private ICollection<TaskModel> Tasks { get; set; }
        private ICollection<Team> Teams { get; set; }
        private ICollection<User> Users { get; set; }

        public AppService(string baseUrl, HttpClient client)
        {
            _projectService = new ProjectService(baseUrl, client);
            _taskService = new TaskService(baseUrl, client);
            _teamService = new TeamService(baseUrl, client);
            _userService = new UserService(baseUrl, client);
        }

        public async Task GetAllDataAsync()
        {
            Teams = (await _teamService.GetTeams()).ToList();
            Users = (await _userService.GetUsers()).ToList();
            Tasks = (await _taskService.GetTasks())
                        .Join(Users,
                            task => task.PerformerId,
                            user => user.Id,
                            (task, user) =>
                            {
                                task.Performer = user;
                                return task;
                            })
                        .ToList();
            Projects = (await _projectService.GetProjects())
                .GroupJoin(Tasks,
                            project => project.Id,
                            task => task.ProjectId,
                            (pr, task) =>
                            {
                                pr.Tasks = task.ToList();
                                return pr;
                            })
                .Join(Teams,
                    project => project.TeamId,
                    team => team.Id,
                    (pr, team) =>
                    {
                        pr.Team = team;
                        return pr;
                    })
                .Join(Users,
                    project => project.AuthorId,
                    author => author.Id,
                    (pr, a) =>
                    {
                        pr.Author = a;
                        return pr;
                    })
                .ToList();
        }

        public Dictionary<Project, int> GetUserProjectTasksCountByUserId(int userId)
        {
            var projects = Projects
                .Where(pr => pr.AuthorId == userId)
                .GroupBy(pr => pr)
                .ToDictionary(item => item.Key, item => item.Key.Tasks.Count());

            return projects;
        }

        public IEnumerable<TaskModel> GetTasksByUserIdWithNameCondition(int userId)
        {
            var tasks = Projects
                .SelectMany(p => p.Tasks
                    .Where(task => task.PerformerId == userId && task.Name.Length < 45)
                );

            return tasks;
        }

        public IEnumerable<TaskFinishedThisYear> GetTasksFinishedIn2020ByUserId(int userId)
        {
            var tasks = Projects
                .SelectMany(p => p.Tasks
                    .Where(task => task.PerformerId == userId && 
                        task.FinishedAt.Year == DateTime.Now.Year/*2020*/ && 
                        task.State == TaskState.Finished)
                    .Select(task => new TaskFinishedThisYear{ Id = task.Id, Name = task.Name })
                );

            return tasks;
        }

        public IEnumerable<Team_Users> GetTeamsUsersSortedByRegisteredDate()
        {
            var teams = Teams
                        .GroupJoin(Users,
                            team => team.Id,
                            user => user.TeamId,
                            (t, u) =>
                            {
                                return new Team_Users
                                {
                                    Id = t.Id,
                                    Name = t.Name,
                                    Users = u
                                        .OrderByDescending(us => us.RegisteredAt)
                                        .ToList()
                                };

                            })
                        .Where(id => id.Users
                                        .All(user => DateTime.Now.Year - user.Birthday.Year > 10) &&
                                        id.Users.Count() != 0);

            return teams;
        }

        public IEnumerable<User_Tasks> GetUsersSortedByFirstNameAndSortedTasks()
        {
            var users = Users
                .GroupJoin(Tasks,
                    user => user.Id,
                    task => task.PerformerId,
                    (u, t) => new User_Tasks
                    {
                        User = u,
                        Tasks = t.OrderByDescending(task => task.Name.Length).ToList()
                    })
                .OrderBy(id => id.User.FirstName);

            return users;
        }

        public IEnumerable<UserLastProjectTasks> CreateNewUserStructure(int userId)
        {
            var structure = Users
                .Where(u => u.Id == userId)
                .GroupJoin(Projects,
                    user => user.Id,
                    project => project.AuthorId,
                    (us, pr) => new UserLastProjectTasks
                    {
                        User = us,
                        LastProject = pr
                            .OrderByDescending(p => p.CreatedAt)
                            .FirstOrDefault(),
                        LastProjectTasksCount = pr
                            .OrderByDescending(p => p.CreatedAt)
                            .FirstOrDefault()?
                            .Tasks.Count(),
                        UndoneOrCanceledTasksCount = Users
                            .Where(user => user.Id == userId)
                            .GroupJoin(Tasks,
                                user => user.Id,
                                task => task.PerformerId,
                                (u, t) => t.Count(task => task.State != TaskState.Finished))
                            .FirstOrDefault(),
                        LongestTask = Users
                            .Where(user => user.Id == userId)
                            .GroupJoin(Tasks,
                                user => user.Id,
                                task => task.PerformerId,
                                (u, t) => t.OrderByDescending(task => task.FinishedAt - task.CreatedAt).FirstOrDefault())
                            .FirstOrDefault(),
                    });

            return structure;
        }

        public IEnumerable<Project_Tasks> CreateNewProjectStructure()
        {
            var structure = Projects
                .Join(Teams,
                    project => project.TeamId,
                    team => team.Id,
                    (pr, t) => new Project_Tasks
                    {
                        Project = pr,
                        LongestTaskByDescription = pr.Tasks
                            .OrderByDescending(task => task.Description.Length).FirstOrDefault(),
                        ShortestTaskByName = pr.Tasks
                            .OrderBy(task => task.Name.Length).FirstOrDefault(),
                        TotalUsersCount = Teams
                        .Where(team => team.Id == t.Id)
                        .GroupJoin(Users,
                            team => team.Id,
                            user => user.TeamId,
                            (t, u) =>
                            {
                                if (pr.Description.Length > 20 || pr.Tasks.Count() < 3)
                                    return u.Count();
                                else
                                    return 0;
                            })
                        .FirstOrDefault()
                    });
            return structure;
        }

        public async Task PrintUserProjectTasksCountByUserIdAsync(int userId)
        {
            var tasks = //(await _projectService.GetUserProjectTasksCountByUserId(userId)).ToList();
                (GetUserProjectTasksCountByUserId(userId));
            if(tasks.Count == 0)
            {
                Console.WriteLine("User is not an author of any project");
                return;
            }
            foreach (var t in tasks)
            {
                Console.WriteLine($"Project:\n[\n{t.Key}\n]\n\nTaskCount:\t{t.Value}\n" +
                $"#########################################\n");
            }
        }

        public async Task PrintTasksByUserIdWithNameConditionAsync(int userId)
        {
            var tasks = (await _taskService.GetTasksByUserIdWithNameCondition(userId)).ToList();
                //GetTasksByUserIdWithNameCondition(userId).ToList();
            if(tasks.Count == 0)
            {
                Console.WriteLine("User is not a performer of any task");
                return;
            }
            Console.WriteLine("Tasks:\n[");
            foreach (var t in tasks)
            {
                Console.WriteLine(t.ToString());
                Console.WriteLine("#################################\n");
            }
            Console.WriteLine("]\n");
        }

        public async Task PrintTasksFinishedIn2020ByUserIdAsync(int userId)
        {
            var tasks = (await _taskService.GetTasksByUserFinishedThisYear(userId)).ToList();
                //GetTasksFinishedIn2020ByUserId(userId).ToList();
            if (!tasks.Any())
            {
                Console.WriteLine("No tasks of this user has been finished in 2020\n");
                return;
            }
            foreach (var t in tasks)
            {
                Console.WriteLine($"id:\t{t.Id}\nname:\t{t.Name}\n");
            }
        }

        public async Task PrintTeamsUsersSortedByRegisteredDateAsync()
        {
            var teams = (await _userService.GetTeamsUsersSortedByRegisteredDate()).ToList();
                //GetTeamsUsersSortedByRegisteredDate().ToList();
            foreach(var t in teams)
            {
                Console.WriteLine($"id:\t{t.Id}\nname:\t{t.Name}\nusers:\n[");
                if (t.Users.Count() == 0)
                {
                    Console.WriteLine("\tNo Users satisfying the condition");
                }
                else
                {
                    foreach (var user in t.Users)
                    {
                        Console.WriteLine("\t" + user.ToString() + "\n");
                    }
                }
                Console.Write("]\t\n\n");
            }
        }

        public async Task PrintUsersSortedByFirstNameAndSortedTasksAsync()
        {
            var users = (await _taskService.GetUsersSortedByFirstNameAndSortedTasks()).ToList();
                //GetUsersSortedByFirstNameAndSortedTasks().ToList();
            foreach (var u in users)
            {
                Console.WriteLine($"user:\n[\n\t{u.User}\n]\ntasks:\n[");
                if (u.Tasks.Count() == 0)
                {
                    Console.WriteLine("\tThis user has no tasks\n");
                }
                else {
                    foreach (var task in u.Tasks)
                    {
                        Console.WriteLine(task.ToString());
                    }
                }

                Console.Write("]\t\n##############################\n");
            }
        }

        public async Task PrintNewUserStructureAsync(int userId)
        {
            var structure = (await _taskService.GetNewUserStructure(userId)).ToList();
                //CreateNewUserStructure(userId).ToList();
            foreach (var s in structure)
            {
                Console.WriteLine($"user:\n[\n\t{s.User}\n]\n\n" +
                    $"last project:\n[\n\t{(s.LastProject == null ? "This user has no project" : s.LastProject.ToString())}\n]\n\n" +
                    $"total tasks count of last project:\t" +
                    $"{(s.LastProjectTasksCount.HasValue ? s.LastProjectTasksCount.ToString() : "As this user has no project, the user has no task of it")}\n\n" +
                    $"undone or canceled tasks count:\t{s.UndoneOrCanceledTasksCount}\n\n" +
                    $"longest task:\n[\n{s.LongestTask}\n]\n##############################\n");
            }
        }

        public async Task PrintNewProjectStructureAsync()
        {
            var structure = (await _projectService.GetNewProjectStructure()).ToList();
                //CreateNewProjectStructure().ToList();
            foreach (var s in structure)
            {
                Console.WriteLine($"project:\n[\n\t{s.Project}\n]\n\n" +
                    $"the longest project task by descriprion:\n[\n{s.LongestTaskByDescription}\n]\n\n" +
                    $"the shortest project task by name:\n[\n{s.ShortestTaskByName}\n]\n\n" +
                    $"total users count in project team:\t{s.TotalUsersCount}\n" +
                    "##############################\n");
            }
        }

        public bool IsUserExist(int userId)
        {
            if (!Users.Where(user => user.Id == userId).Any())
            {
                Console.WriteLine("User with such id doesn't exist!");
                return false;
            }
            return true;
        }
    }
}
