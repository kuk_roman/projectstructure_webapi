﻿using Collections_LINQ.Interfaces;
using Collections_LINQ.Models;
using Collections_LINQ.Models.FunctionModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ.Services
{
    class TaskService : ITaskService
    {
        private readonly string baseUrl;
        private readonly HttpClient client;

        public TaskService(string baseUrl, HttpClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<TaskModel> GetTaskById(int id)
        {
            var task = await client.GetStringAsync($"{baseUrl}/api/tasks/{id}");
            return JsonConvert.DeserializeObject<TaskModel>(task);
        }

        public async Task<ICollection<TaskModel>> GetTasks()
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks");
            return JsonConvert.DeserializeObject<ICollection<TaskModel>>(tasks);
        }

        public async Task<TaskModel> CreateTask(TaskModel task)
        {
            var response = await client.PostAsJsonAsync($"{baseUrl}/api/tasks", task);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TaskModel>();
        }

        public async Task<TaskModel> UpdateTask(TaskModel task)
        {
            var response = await client.PutAsJsonAsync($"{baseUrl}/api/tasks", task);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<TaskModel>();
        }

        public async Task DeleteTask(string id)
        {
            var response = await client.DeleteAsync($"{baseUrl}/api/tasks/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
        }

        public async Task<IEnumerable<TaskModel>> GetTasksByUserIdWithNameCondition(int id)
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks/by-user-with-name-condition/{id}");
            return JsonConvert.DeserializeObject<IEnumerable < TaskModel >> (tasks);
        }

        public async Task<IEnumerable<TaskFinishedThisYear>> GetTasksByUserFinishedThisYear(int id)
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks/by-user-finished-this-year/{id}");
            return JsonConvert.DeserializeObject<IEnumerable<TaskFinishedThisYear>>(tasks);
        }

        public async Task<IEnumerable<User_Tasks>> GetUsersSortedByFirstNameAndSortedTasks()
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks/users-sorted-by-first-name-sorted-tasks");
            return JsonConvert.DeserializeObject<IEnumerable<User_Tasks>>(tasks);
        }

        public async Task<IEnumerable<UserLastProjectTasks>> GetNewUserStructure(int id)
        {
            var tasks = await client.GetStringAsync($"{baseUrl}/api/tasks/structure-with-tasks-by-user/{id}");
            return JsonConvert.DeserializeObject<IEnumerable<UserLastProjectTasks>>(tasks);
        }
    }
}
