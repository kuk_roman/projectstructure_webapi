﻿using Collections_LINQ.Interfaces;
using Collections_LINQ.Models;
using Collections_LINQ.Models.FunctionModels;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ.Services
{
    class ProjectService: IProjectService
    {
        private readonly string baseUrl;
        private readonly HttpClient client;

        public ProjectService(string baseUrl, HttpClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<ICollection<Project>> GetProjects()
        {
            var projects = await client.GetStringAsync($"{baseUrl}/api/projects");
            return JsonConvert.DeserializeObject<ICollection<Project>>(projects);
        }

        public async Task<Project> GetProjectById(int id)
        {
            var project = await client.GetStringAsync($"{baseUrl}/api/projects/{id}");
            return JsonConvert.DeserializeObject<Project>(project);
        }

        public async Task<Project> CreateProject(Project project)
        {
            var response = await client.PostAsJsonAsync($"{baseUrl}/api/projects", project);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<Project>();
        }

        public async Task<Project> UpdateProject(Project project)
        {
            var response = await client.PutAsJsonAsync($"{baseUrl}/api/projects", project);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<Project>();
        }

        public async Task DeleteProject(string id)
        {
            var response = await client.DeleteAsync($"{baseUrl}/api/projects/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
        }

        public async Task<IEnumerable<Project_Tasks>> GetNewProjectStructure()
        {
            var projects = await client.GetStringAsync($"{baseUrl}/api/projects/new-project-structure");
            return JsonConvert.DeserializeObject<IEnumerable<Project_Tasks>>(projects);
        }

        public async Task<Dictionary<Project, int>> GetUserProjectTasksCountByUserId(int id)
        {
            var projects = await client.GetStringAsync($"{baseUrl}/api/projects/project-tasks-count-by-user/{id}");
            return JsonConvert.DeserializeObject<Dictionary<Project, int>> (projects);
        }
    }

}
