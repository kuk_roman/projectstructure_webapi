﻿using Collections_LINQ.Interfaces;
using Collections_LINQ.Models;
using Collections_LINQ.Models.FunctionModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ.Services
{
    class UserService : IUserService
    {
        private readonly string baseUrl;
        private readonly HttpClient client;

        public UserService(string baseUrl, HttpClient client)
        {
            this.baseUrl = baseUrl;
            this.client = client;
        }

        public async Task<ICollection<User>> GetUsers()
        {
            var users = await client.GetStringAsync($"{baseUrl}/api/users");
            return JsonConvert.DeserializeObject<ICollection<User>>(users);
        }

        public async Task<User> GetUserById(int id)
        {
            var user = await client.GetStringAsync($"{baseUrl}/api/users/{id}");
            return JsonConvert.DeserializeObject<User>(user);
        }

        public async Task<User> CreateUser(User user)
        {
            var response = await client.PostAsJsonAsync($"{baseUrl}/api/users", user);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<User>();
        }

        public async Task<User> UpdateUser(User user)
        {
            var response = await client.PutAsJsonAsync($"{baseUrl}/api/users", user);
            if (response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<User>();
        }

        public async Task DeleteUser(string id)
        {
            var response = await client.DeleteAsync($"{baseUrl}/api/users/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
        }

        public async Task<IEnumerable<Team_Users>> GetTeamsUsersSortedByRegisteredDate()
        {
            var users = await client.GetStringAsync($"{baseUrl}/api/users/team-users-sorted-by-registered-date");
            return JsonConvert.DeserializeObject<IEnumerable<Team_Users>>(users);
        }
    }
}
