﻿using Collections_LINQ.Services;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Collections_LINQ
{
    class Program
    {
        private static readonly string _baseUrl = Resource.Uri;
            //"https://bsa20.azurewebsites.net";

        static async Task Main(string[] args)
        {
            var httpClient = new HttpClient();
            var appClient = new AppService(_baseUrl, httpClient);
            await appClient.GetAllDataAsync();

            while (true)
            {
                Console.WriteLine("Select a command:");
                Console.WriteLine("1 - Print task count per project of certain user");
                Console.WriteLine("2 - Print tasks of certain user (length of task name < 45)");
                Console.WriteLine("3 - Print tasks (id, name) of certain user which have been finished in the current (2020) year");
                Console.WriteLine("4 - Print list of teams (id, name, users), users age > 10, " +
                                "sorted in descending order by registered date, grouped by teams");
                Console.WriteLine("5 - Print users sorted by first name whose tasks are sorted in descending order by length of name");
                Console.WriteLine("6 - Print structure:\n\tUser\n\tLast Project\n\tOverall tasks count of last project\n\t" +
                                "Overall count of undone or canceled tasks\n\tThe longest task");
                Console.WriteLine("7 - Print structure:\n\tProject\n\tThe longest project task by description\n\t" +
                                "The shortest project task by name\n\tOverall users count in project team");
                Console.WriteLine("0 - Exit");
                Console.WriteLine();

                var commandString = Console.ReadLine();

                Console.WriteLine();

                try
                {
                    int command = int.Parse(commandString);
                    int id;
                    switch (command)
                    {
                        case 0:
                            return;
                        case 1:
                            Console.WriteLine("Input id of a user:\t");
                            id = int.Parse(Console.ReadLine());
                            Console.WriteLine();
                            if (appClient.IsUserExist(id))
                                await appClient.PrintUserProjectTasksCountByUserIdAsync(id);
                            break;
                        case 2:
                            Console.WriteLine("Input id of a user:\t");
                            id = int.Parse(Console.ReadLine());
                            Console.WriteLine();
                            if (appClient.IsUserExist(id))
                                await appClient.PrintTasksByUserIdWithNameConditionAsync(id);
                            break;
                        case 3:
                            Console.WriteLine("Input id of a user:\t");
                            id = int.Parse(Console.ReadLine());
                            Console.WriteLine();
                            if (appClient.IsUserExist(id))
                                await appClient.PrintTasksFinishedIn2020ByUserIdAsync(id);
                            break;
                        case 4:
                            await appClient.PrintTeamsUsersSortedByRegisteredDateAsync();
                            break;
                        case 5:
                            await appClient.PrintUsersSortedByFirstNameAndSortedTasksAsync();
                            break;
                        case 6:
                            Console.WriteLine("Input id of a user:\t");
                            id = int.Parse(Console.ReadLine());
                            Console.WriteLine();
                            if (appClient.IsUserExist(id))
                                await appClient.PrintNewUserStructureAsync(id);
                            break;
                        case 7:
                            await appClient.PrintNewProjectStructureAsync();
                            break;
                        default:
                            throw new FormatException();
                    }

                }
                catch (FormatException)
                {
                    Console.WriteLine($"Incorrect command");
                }
                Console.WriteLine();
            }
        }
    }
}
