﻿using System.Collections.Generic;

namespace Collections_LINQ.Models.FunctionModels
{
    class User_Tasks
    {
        public User User { get; set; }
        public List<TaskModel> Tasks { get; set; }
    }
}
