﻿using System.Collections.Generic;

namespace Collections_LINQ.Models.FunctionModels
{
    class Team_Users
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<User> Users { get; set; } 
    }
}
