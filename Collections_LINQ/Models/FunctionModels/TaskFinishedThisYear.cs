﻿

namespace Collections_LINQ.Models.FunctionModels
{
    class TaskFinishedThisYear
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
