﻿namespace Collections_LINQ.Models
{
    enum TaskState
    {
        Created,
        Started,
        Finished,
        Canceled
    }
}
