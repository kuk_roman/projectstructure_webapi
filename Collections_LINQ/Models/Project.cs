﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Collections_LINQ.Models
{
    class Project
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("description")]
        public string? Description { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }

        [JsonProperty("authorId")]
        public int AuthorId { get; set; }

        [JsonProperty("teamId")]
        public int TeamId { get; set; }

        public ICollection<TaskModel> Tasks { get; set; }

        public Team? Team { get; set; }

        public User? Author { get; set; }

        public override string ToString()
        {
            return string.Format($"id:\t{Id}\n" +
                $"name:\t{Name ?? "This project has no name"}\n\n" +
                $"description:\t{Description ?? "This project has no description"}\n\n" +
                $"createdAt:\t{CreatedAt}\ndeadline:\t{Deadline}\ntasks:\n[\n" +
                $"{(Tasks.Count == 0 ? "\tThis project has no tasks\n" : Tasks.Aggregate("", (cur, next) => cur += next))}]\n\n" +
                $"author:\n[\n\t{Author}\n]\n\nteam:\n[\n\t{Team}\n]\n");
        }
    }
}
