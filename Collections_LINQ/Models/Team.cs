﻿using Newtonsoft.Json;
using System;

namespace Collections_LINQ.Models
{
    class Team
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string? Name { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        public override string ToString()
        {
            return string.Format($"id:\t{Id}\n\t" +
                $"name:\t{Name ?? "This team has no name"}\n\t" +
                $"createdAt:\t{CreatedAt}");
        }
    }
}
