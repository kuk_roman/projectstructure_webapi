﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public sealed class TaskStateModel : BaseEntity
    {
        public string? Value { get; set; }
    }
}
