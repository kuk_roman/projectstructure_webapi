﻿using DAL.Entities.Abstract;
using System;

namespace DAL.Entities
{
    public sealed class Team: BaseEntity
    {
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
