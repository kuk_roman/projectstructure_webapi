﻿using DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Entities
{
    public sealed class Project : BaseEntity
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }
        public int AuthorId { get; set; }
        public int TeamId { get; set; }

        public ICollection<TaskModel> Tasks { get; set; }
        public Team? Team { get; set; }
        public User? Author { get; set; }
    }
}
