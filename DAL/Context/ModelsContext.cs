﻿using Bogus;
using DAL.Entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DAL.Context
{
    public sealed class ModelsContext
    {

        public IEnumerable<Project> Projects { get; private set; } = new List<Project>();
        public IEnumerable<TaskModel> Tasks { get; private set; } = new List<TaskModel>();
        public IEnumerable<Team> Teams { get; private set; } = new List<Team>();
        public IEnumerable<User> Users { get; private set; } = new List<User>();

        public async Task SerializeAsync()
        {
            await Task.Factory.StartNew(() =>
            {
                using StreamWriter sw = File.CreateText("data.json");
                new JsonSerializer().Serialize(sw, this);
            });
        }

        public void Serialize()
        {
            using StreamWriter sw = File.CreateText("data.json");
            new JsonSerializer().Serialize(sw, this);
        }

        public ModelsContext Deserialize()
        {
            try
            { 
                using StreamReader sr = File.OpenText("data.json");
                var modelsContext = (ModelsContext)new JsonSerializer().Deserialize(sr, typeof(ModelsContext));
                Teams = modelsContext.Teams;
                Users = modelsContext.Users;
                Projects = modelsContext.Projects;
                Tasks = modelsContext.Tasks;
            }
            catch (FileNotFoundException)
            {
                Randomizer.Seed = new Random(1);

                Teams = ModelBuilderExtensions.GenerateRandomTeams();
                Users = ModelBuilderExtensions.GenerateRandomUsers(Teams);
                Projects = ModelBuilderExtensions.GenerateRandomProjects(Users, Teams);
                Tasks = ModelBuilderExtensions.GenerateRandomTasks(Users, Projects);
                Tasks = Tasks
                            .Join(Users,
                                task => task.PerformerId,
                                user => user.Id,
                                (task, user) =>
                                {
                                    task.Performer = user;
                                    return task;
                                });
                //Projects = Projects
                //    .GroupJoin(Tasks,
                //            project => project.Id,
                //            task => task.ProjectId,
                //            (pr, task) =>
                //            {
                //                pr.Tasks = task.ToList();
                //                return pr;
                //            });
                Projects = Projects
                .GroupJoin(Tasks,
                            project => project.Id,
                            task => task.ProjectId,
                            (pr, task) =>
                            {
                                pr.Tasks = task.ToList();
                                return pr;
                            })
                .Join(Teams,
                    project => project.TeamId,
                    team => team.Id,
                    (pr, team) =>
                    {
                        pr.Team = team;
                        return pr;
                    })
                .Join(Users,
                    project => project.AuthorId,
                    author => author.Id,
                    (pr, a) =>
                    {
                        pr.Author = a;
                        return pr;
                    });

                Serialize();
                Deserialize();
            }
            return this;
        }

        public List<TEntity> Set<TEntity>() where TEntity : class
        {
            return (typeof(TEntity).ToString()) switch
            {
                "DAL.Entities.Team" => (List<TEntity>)Teams,
                "DAL.Entities.User" => (List<TEntity>)Users,
                "DAL.Entities.Project" => (List<TEntity>)Projects,
                "DAL.Entities.TaskModel" => (List<TEntity>)Tasks,
                _ => new List<TEntity>(),
            };
        }
    }
}
