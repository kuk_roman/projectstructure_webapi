﻿using Bogus;
using DAL.Entities;
using System;
using System.Collections.Generic;

namespace DAL.Context
{
    public static class ModelBuilderExtensions
    {
        private const int TEAMS_COUNT = 10;
        private const int USERS_COUNT = 100;
        private const int PROJECT_COUNT = 50;
        private const int TASKS_COUNT = 200;

        public static IEnumerable<User> GenerateRandomUsers(IEnumerable<Team> teams)
        {
            int userId = 1;

            var usersFake = new Faker<User>()
                .RuleFor(u => u.Id, f => userId++)
                .RuleFor(u => u.FirstName, f => f.Person.FirstName)
                .RuleFor(u => u.LastName, f => f.Person.LastName)
                .RuleFor(u => u.Email, f => f.Person.Email)
                .RuleFor(pi => pi.Birthday, f => f.Date.Between(new DateTime(1990, 1, 1), new DateTime(2010, 1, 1)))
                .RuleFor(pi => pi.RegisteredAt, f => DateTime.Now)
                .RuleFor(pi => pi.TeamId, f => f.PickRandom(teams).Id);

            return usersFake.Generate(USERS_COUNT);
        }

        public static IEnumerable<Team> GenerateRandomTeams()
        {
            int teamId = 1;

            var teamsFake = new Faker<Team>()
                .RuleFor(p => p.Id, f => teamId++)
                .RuleFor(p => p.Name, f => f.Company.CompanyName())
                .RuleFor(pi => pi.CreatedAt, f => f.Date.Between(new DateTime(2000,1,1), DateTime.Now));

            return teamsFake.Generate(TEAMS_COUNT);
        }

        public static IEnumerable<TaskModel> GenerateRandomTasks(IEnumerable<User> users, IEnumerable<Project> projects)
        {
            int taskId = 1;

            var tasksFake = new Faker<TaskModel>()
                .RuleFor(c => c.Id, f => taskId++)
                .RuleFor(c => c.Name, f => f.Lorem.Sentence())
                .RuleFor(c => c.Description, f => f.Lorem.Sentences())
                .RuleFor(c => c.ProjectId, f => f.PickRandom(projects).Id)
                .RuleFor(c => c.PerformerId, f => f.PickRandom(users).Id)
                .RuleFor(c => c.State, f => f.Random.Enum<TaskState>())
                .RuleFor(pi => pi.CreatedAt, f => f.Date.Between(new DateTime(2015, 1, 1), new DateTime(2019, 1, 1)))
                .RuleFor(pi => pi.FinishedAt, f => f.Date.Future(5, new DateTime(2019, 1, 1)));

            return tasksFake.Generate(TASKS_COUNT);
        }

        public static IEnumerable<Project> GenerateRandomProjects(IEnumerable<User> users, IEnumerable<Team> teams)
        {
            int projectId = 1;

            var projectsFake = new Faker<Project>()
                .RuleFor(c => c.Id, f => projectId++)
                .RuleFor(c => c.Name, f => f.Commerce.ProductName())
                .RuleFor(c => c.Description, f => f.Lorem.Sentences())
                .RuleFor(c => c.AuthorId, f => f.PickRandom(users).Id)
                .RuleFor(c => c.TeamId, f => f.PickRandom(teams).Id)
                .RuleFor(pi => pi.CreatedAt, f => f.Date.Between(new DateTime(2015, 1, 1), new DateTime(2019, 1, 1)))
                .RuleFor(pi => pi.Deadline, f => f.Date.Future(5, new DateTime(2019, 1, 1)));

            return projectsFake.Generate(PROJECT_COUNT);
        }
    }
}
